

#include "asf.h"


/************************************************************************/
/* defines                                                              */
/************************************************************************/

/*
#define LED_PIO PIOC
#define LED_PIO_ID ID_PIOC
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define BUT_PIO PIOA
#define BUT_PIO_ID  ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 <<  BUT_PIO_PIN)

*/
//======

//MIDI Config
#define NOTE_ON_CMD 0x90
#define NOTE_OFF_CMD 0x80
#define NOTE_VELOCITY 127

//entrada1
#define key_in1_id ID_PIOC
#define key_in1_pio PIOC
#define key_in1_pin  31
#define key_in1_mask (1 << key_in1_pin)
//entrada2
#define key_in2_id ID_PIOB
#define key_in2_pio PIOB
#define key_in2_pin  31
#define key_in2_mask (1 << key_in2_pin)
//entrada3
#define key_in3_id ID_PIOA
#define key_in3_pio PIOA
#define key_in3_pin  0
#define key_in3_mask (1 << key_in3_pin)
//entrada4
#define key_in4_id ID_PIOD
#define key_in4_pio PIOD
#define key_in4_pin  28
#define key_in4_mask (1 << key_in4_pin)
//entrada5
#define key_in5_id ID_PIOA
#define key_in5_pio PIOA
#define key_in5_pin  3
#define key_in5_mask (1 << key_in5_pin)
//entrada6
#define key_in6_id ID_PIOB
#define key_in6_pio PIOB
#define key_in6_pin  0
#define key_in6_mask (1 << key_in6_pin)

//saida1
#define key_out1_id ID_PIOC
#define key_out1_pio PIOC
#define key_out1_pin  13
#define key_out1_mask (1 << key_out1_pin)
//saida2
#define key_out2_id ID_PIOD
#define key_out2_pio PIOD
#define key_out2_pin  11
#define key_out2_mask (1 << key_out2_pin)
//saida3
#define key_out3_id ID_PIOD
#define key_out3_pio PIOD
#define key_out3_pin  26
#define key_out3_mask (1 << key_out3_pin)
//saida4
#define key_out4_id ID_PIOA
#define key_out4_pio PIOA
#define key_out4_pin  24
#define key_out4_mask (1 << key_out4_pin)
//saida5
#define key_out5_id ID_PIOA
#define key_out5_pio PIOA
#define key_out5_pin  4
#define key_out5_mask (1 << key_out5_pin)
//saida6
#define key_out6_id ID_PIOB
#define key_out6_pio PIOB
#define key_out6_pin  4
#define key_out6_mask (1 << key_out6_pin)
//saida7
#define key_out7_id ID_PIOB
#define key_out7_pio PIOB
#define key_out7_pin  0
#define key_out7_mask (1 << key_out7_pin)
//saida8
#define key_out8_id ID_PIOD
#define key_out8_pio PIOD
#define key_out8_pin  25
#define key_out8_mask (1 << key_out8_pin)
//saida9
#define key_out9_id ID_PIOD
#define key_out9_pio PIOD
#define key_out9_pin  20
#define key_out9_mask (1 << key_out9_pin)
//saidaMidi
#define midi_id ID_PIOD
#define midi_pio PIOD
#define midi_pin  30
#define midi_mask (1 << midi_pin)

/** UART Interface */
#define CONF_UART            CONSOLE_UART
/** Baudrate setting */
#define CONF_UART_BAUDRATE   (31250UL)
/** Character length setting */
#define CONF_UART_CHAR_LENGTH  US_MR_CHRL_8_BIT
/** Parity setting */
#define CONF_UART_PARITY     US_MR_PAR_NO
/** Stop bits setting */
#define CONF_UART_STOP_BITS    US_MR_NBSTOP_1_BIT

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/
uint8_t keys[49];
int pressed = 0;
int counter = 1;
int flag_test = 0;


char keysDic[6][9][4] = {
	{//1
		"F4#", "C5", "C7", "F5#", "C3", "C6", "F3#", "F6#", "C4"
	},//2
	{
		"F4", "B4", "B7", "F5", "NoD", "B5", "F3", "F6", "B3"		
	},//3
	{
		"E4", "A4#", "A7#", "E5", "NoD", "A5#", "E3", "E6", "A3#"
	},//4
	{
		"D4#", "A4", "A7", "D5#", "NoD", "A5", "D3#", "D6#", "A3"
	},//5
	{
		"D4", "G4#", "G6#", "D5", "NoD", "G5#", "D3", "D6", "G3#"
	},//6
	{
		"C4#", "G4", "G6", "C5#", "NoD", "G5", "C3#", "C6#", "G3"
	},
};

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/
/*
static void configure_console(void)
{

	// Configura USART1 Pinos 
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4);  // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;
	
	const usart_serial_options_t uart_serial_options = {
		.baudrate   = CONF_UART_BAUDRATE,
		.charlength = CONF_UART_CHAR_LENGTH,
		.paritytype = CONF_UART_PARITY,
		.stopbits   = CONF_UART_STOP_BITS,
	};

	// Configure console UART. 
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}
*/

static void USART0_init(void){
	
	/* Configura USART0 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB0);
	pio_set_peripheral(PIOB, PIO_PERIPH_C, PIO_PB1);
	
	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate     = 31250,
		.char_length  = US_MR_CHRL_8_BIT,
		.parity_type  = US_MR_PAR_NO,
		.stop_bits    = US_MR_NBSTOP_1_BIT,
		.channel_mode = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART0 */
	sysclk_enable_peripheral_clock(ID_USART0);
	
	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART0, &usart_settings, sysclk_get_peripheral_hz());
	
	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART0);
	usart_enable_rx(USART0);
}

//---Detection---
/*
void getActiveColumns(int matrix[6][9], int row ) {
	int tempMatrix[9] = {
		pio_get(out1_pio,PIO_INPUT,out1_mask),
		pio_get(out2_pio,PIO_INPUT,out2_mask),
		pio_get(out3_pio,PIO_INPUT,out3_mask),
		pio_get(out4_pio,PIO_INPUT,out4_mask),
		pio_get(out5_pio,PIO_INPUT,out5_mask),
		pio_get(out6_pio,PIO_INPUT,out6_mask) & 0,
		pio_get(out7_pio,PIO_INPUT,out7_mask),
		pio_get(out8_pio,PIO_INPUT,out8_mask),
		pio_get(out9_pio,PIO_INPUT,out9_mask)
	};
	
	int tmp;
	
	tmp = pio_get(out2_pio,PIO_INPUT,out2_mask);
	
	for(int i = 0; i < 9; i ++){
		matrix[row][i] = tempMatrix[i];
	}
};
*/

//-----NEW 10/06/18-----

/*
void SendByteMidi(char byte){
	pio_clear(midi_pio,midi_mask);
	delay_us(32);
	for (int i = 0; i < 8; i ++){
		if(byte >> i & 0x01){
			pio_set(midi_pio,midi_mask);
		} else {
			pio_clear(midi_pio,midi_mask);
		}
		delay_us(32);
	}
	delay_us(32);
	pio_set(midi_pio,midi_mask);

}
*/

void NoteOn(char note){
  usart_putchar(USART0,NOTE_ON_CMD);
  usart_putchar(USART0,note);
  usart_putchar(USART0,NOTE_VELOCITY);
  /*
  SendByteMidi(NOTE_ON_CMD);
  SendByteMidi(note);
  SendByteMidi(NOTE_VELOCITY);
  */
}

void NoteOff(char note){
  usart_putchar(USART0,NOTE_OFF_CMD);
  usart_putchar(USART0,note);
  usart_putchar(USART0,NOTE_VELOCITY);
/*
	SendByteMidi(NOTE_OFF_CMD);
	SendByteMidi(note);
	SendByteMidi(NOTE_VELOCITY);
	*/
}


//-----END NEW-----

void DetecKey(){

		//---detect C3 test---
		NoteOn(60);
		/*
		pio_set(key_in1_pio,key_in1_mask);
		if(pio_get(key_out2_pio,PIO_INPUT,key_out2_mask)){
			//if(!flag_test){
				NoteOn(60);
				flag_test = 1;
			//}
			
		} else {
			//if(flag_test){
				NoteOff(60);
				flag_test = 0;
			//}
		}
		pio_clear(key_in1_pio,key_in1_mask);
		
/*    //////////////////////////////////pio_get(out1_pio,PIO_INPUT,out1_mask),
		int keyMatrix[6][9];
		
		//pio_set(in1_pio,in1_mask);
		//delay_ms(1);
		//getActiveColumns(keyMatrix,0);
		//pio_clear(in1_pio,in1_mask);
		//delay_ms(1);
		
		pio_set(in2_pio,in2_mask);
		delay_ms(1);
		getActiveColumns(keyMatrix,1);
		pio_clear(in2_pio,in2_mask);
		delay_ms(1);
		
		//pio_set(in3_pio,in3_mask);
		//delay_ms(1);
		//getActiveColumns(keyMatrix,2);
		//pio_clear(in3_pio,in3_mask);
		//delay_ms(1);
		//
		//pio_set(in4_pio,in4_mask);
		//delay_ms(1);
		//getActiveColumns(keyMatrix,3);
		//pio_clear(in4_pio,in4_mask);
		//delay_ms(1);
		//
		//pio_set(in5_pio,in5_mask);
		//delay_ms(1);
		//getActiveColumns(keyMatrix,4);
		//pio_clear(in5_pio,in5_mask);
		//delay_ms(1);
		//
		//pio_set(in6_pio,in6_mask);
		//delay_ms(1);
		//getActiveColumns(keyMatrix,5);
		//pio_clear(in6_pio,in6_mask);
		//delay_ms(1);
		
		/*
		for(int i = 0; i < 6; i ++){
			for (int j = 0; j < 9; j ++){
				if(keyMatrix[i][j]){
					printf("Apertou a trecla : %s \n",keysDic[i][j]);	
				}
			}
		}
		*/
		
	
	}

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	sysclk_init();
	//board_init();
	WDT->WDT_MR = WDT_MR_WDDIS;
	//configuração led
	//pmc_enable_periph_clk(LED_PIO_ID);
	//pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	// configuração botão
	//pmc_enable_periph_clk(BUT_PIO_ID);
	//pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	//configurando uart
	//configure_console();
	//configuração do PIO
		
	pmc_enable_periph_clk(PIOA);
	pmc_enable_periph_clk(PIOB);
	pmc_enable_periph_clk(PIOC);
	pmc_enable_periph_clk(PIOD);
		
		
	//Set all ins to keyboard
	pio_set_output(key_in1_pio, key_in1_mask, 0, 0, 0 );
	pio_set_output(key_in2_pio, key_in2_mask, 0, 0, 0 );
	pio_set_output(key_in3_pio, key_in3_mask, 0, 0, 0 );
	pio_set_output(key_in4_pio, key_in4_mask, 0, 0, 0 );
	pio_set_output(key_in5_pio, key_in5_mask, 0, 0, 0 );
	pio_set_output(key_in6_pio, key_in6_mask, 0, 0, 0 );

	//set midi port
	pio_set_output(midi_pio, midi_mask, 0, 0, 0 );
	pio_set(midi_pio,midi_mask); //set for reference

	//set USART0 to midiport
	USART0_init();
		
	//Set all outs from keyboard
	pio_set_input(key_out1_pio, key_out1_mask, PIO_DEBOUNCE);
	//pio_set_debounce_filter(key_out1_pio, key_out1_mask,120);
	pio_pull_down(key_out1_pio, key_out1_mask,1);
		
		
	pio_set_input(key_out2_pio, key_out2_mask, PIO_DEBOUNCE);
	//pio_set_debounce_filter(key_out2_pio, key_out2_mask,120);
	pio_pull_down(key_out2_pio, key_out2_mask,1);
	//pio_configure(key_out2_pio, PIO_INPUT, key_out3_mask, PIO_DEFAULT);
		
	pio_set_input(key_out3_pio, key_out3_mask, PIO_DEBOUNCE);
	//pio_set_debounce_filter(key_out3_pio, key_out3_mask,120);
	pio_pull_down(key_out3_pio, key_out3_mask,1);
		
	pio_set_input(key_out4_pio, key_out4_mask, PIO_DEBOUNCE);
	//p/io_set_debounce_filter(key_out4_pio, key_out4_mask,120);
	pio_pull_down(key_out4_pio, key_out4_mask,1);
		
	pio_set_input(key_out5_pio, key_out5_mask, PIO_DEBOUNCE);
	//pio_set_debounce_filter(key_out5_pio, key_out5_mask,120);
	pio_pull_down(key_out5_pio, key_out5_mask,1);
		
	pio_set_input(key_out6_pio, key_out6_mask, PIO_DEBOUNCE);
	//pio_set_debounce_filter(key_out6_pio, key_out6_mask,120);
	pio_pull_down(key_out6_pio, key_out6_mask,1);
		
	pio_set_input(key_out7_pio, key_out7_mask, PIO_DEBOUNCE);
	////pio_set_debounce_filter(key_out7_pio, key_out7_mask,120);
	pio_pull_down(key_out7_pio, key_out7_mask,1);
		
	pio_set_input(key_out8_pio, key_out8_mask, PIO_DEBOUNCE);
	////pio_set_debounce_filter(key_out8_pio, key_out8_mask,120);
	pio_pull_down(key_out8_pio, key_out8_mask,1);
		
	pio_set_input(key_out9_pio, key_out9_mask, PIO_DEBOUNCE);
	//pio_set_debounce_filter(key_out9_pio, key_out9_mask,120);
	pio_pull_down(key_out9_pio, key_out9_mask,1);

	//printf("---DEBUG START---\n");
	
	// loop
	while (1) {
		
		/*
		NoteOn(60);
		*/

		
		NoteOn(60);
		delay_ms(300);
		NoteOff(60);
		delay_ms(300);

		


	}
	return 0;
}
