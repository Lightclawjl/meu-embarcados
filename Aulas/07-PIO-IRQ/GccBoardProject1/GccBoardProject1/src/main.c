#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
	
#define LED_PIO PIOA
#define LED_PIO_ID ID_PIOA
#define LED_PIO_PIN 4
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOB
#define BUT_PIO_ID ID_PIOB
#define BUT_PIO_PIN 2
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

#define BUZ_PIO PIOB
#define BUZ_PIO_ID ID_PIOB
#define BUZ_PIO_PIN 3
#define BUZ_PIO_PIN_MASK (1 << BUZ_PIO_PIN)

#define INF_PIO PIOA
#define INF_PIO_ID ID_PIOA
#define INF_PIO_PIN 3
#define INF_PIO_PIN_MASK (1 << INF_PIO_PIN)

#define STANDARDINCREASE 50
#define HAPPINESSCYCLE 200 //define em milissegundos quanto dura um ponto de felicidade



// //MUSIC STUFF aqui antes
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

//Meus defines para os tons que a gente usar
#define SADBEEP 0
#define HAPPYBEEP 1
#define THANKYOUBEEP 2

int sadbeep[]= {

	NOTE_GS5, NOTE_G5, NOTE_F5, NOTE_CS5, NOTE_AS4,	
};

int sadtempo[]= {

	6, 12, 12, 12, 12
};

int happybeep[]= {

	NOTE_C6, NOTE_G6, NOTE_F6, NOTE_AS6,
};

int happytempo[]= {

	6, 12, 12, 12,
};

int thankyoubeep[]= {
	NOTE_B5, NOTE_D6, NOTE_D6,	
};

int thankyoutempo[]= {

	6, 12, 12,
};

int melody[] = {
	NOTE_E7, NOTE_E7, 0, NOTE_E7,
	0, NOTE_C7, NOTE_E7, 0,
	NOTE_G7, 0, 0,  0,
	NOTE_G6, 0, 0, 0,
	
	NOTE_C7, 0, 0, NOTE_G6,
	0, 0, NOTE_E6, 0,
	0, NOTE_A6, 0, NOTE_B6,
	0, NOTE_AS6, NOTE_A6, 0,
	
	NOTE_G6, NOTE_E7, NOTE_G7,
	NOTE_A7, 0, NOTE_F7, NOTE_G7,
	0, NOTE_E7, 0, NOTE_C7,
	NOTE_D7, NOTE_B6, 0, 0,
	
	NOTE_C7, 0, 0, NOTE_G6,
	0, 0, NOTE_E6, 0,
	0, NOTE_A6, 0, NOTE_B6,
	0, NOTE_AS6, NOTE_A6, 0,
	
	NOTE_G6, NOTE_E7, NOTE_G7,
	NOTE_A7, 0, NOTE_F7, NOTE_G7,
	0, NOTE_E7, 0, NOTE_C7,
	NOTE_D7, NOTE_B6, 0, 0
};

//Mario main them tempo
int tempo[] = {
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	9, 9, 9,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	
	9, 9, 9,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
};

void buzz(long frequency, long length) {
	long delayValue = 1000000 / frequency / 2; // calculate the delay value between transitions
	//// 1 second's worth of microseconds, divided by the frequency, then split in half since
	//// there are two phases to each cycle
	long numCycles = frequency * length / 1000; // calculate the number of cycles for proper timing
	//// multiply frequency, which is really cycles per second, by the number of seconds to
	//// get the total number of cycles to produce
	for (long i = 0; i < numCycles; i++) { // for the calculated length of time...
		pio_set(BUZ_PIO, BUZ_PIO_PIN_MASK); // write the buzzer pin high to push out the diaphram
		delay_us(delayValue); // wait for the calculated delay value
		pio_clear(BUZ_PIO, BUZ_PIO_PIN_MASK); // write the buzzer pin low to pull back the diaphram
		delay_us(delayValue); // wait again or the calculated delay value
	}
}

void sing(int song) {
	// iterate over the notes of the melody:
	int size;
	switch(song){
		case SADBEEP:
			size= sizeof(sadbeep) / sizeof(int);
			for(int thisNote= 0; thisNote < size; thisNote++){
				int noteDuration = 1000 / sadtempo[thisNote];
				buzz(sadbeep[thisNote], noteDuration);
				
				int pauseBetweenNotes = noteDuration * 0.4;
				delay_ms(pauseBetweenNotes);
				
				buzz(0, noteDuration);
			}
			break;
		
		case HAPPYBEEP:
			size= sizeof(happybeep) / sizeof(int);
			for(int thisNote= 0; thisNote < size; thisNote++){
				int noteDuration = 1000 / happytempo[thisNote];
				buzz(happybeep[thisNote], noteDuration);
				
				int pauseBetweenNotes = noteDuration * 0.4;
				delay_ms(pauseBetweenNotes);
				
				buzz(0, noteDuration);
			}
			break;
			
		case THANKYOUBEEP:
			size= sizeof(thankyoubeep) / sizeof(int);
			for(int thisNote= 0; thisNote < size; thisNote++){
				int noteDuration = 1000 / thankyoutempo[thisNote];
				buzz(thankyoubeep[thisNote], noteDuration);
				
				int pauseBetweenNotes = noteDuration * 0.4;
				delay_ms(pauseBetweenNotes);
				
				buzz(0, noteDuration);
			}
			break;
			
		default:
			//Mario, usar pra testes
			size= sizeof(melody) / sizeof(int);
			for (int thisNote = 0; thisNote < size; thisNote++) {
				
				// to calculate the note duration, take one second
				// divided by the note type.
				//e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
				int noteDuration = 1000 / tempo[thisNote];
				
				buzz(melody[thisNote], noteDuration);
				
				// to distinguish the notes, set a minimum time between them.
				// the note's duration + 30% seems to work well:
				int pauseBetweenNotes = noteDuration * 1.30;
				delay_ms(pauseBetweenNotes);
				
				// stop the tone playing:
				buzz(0, noteDuration);
			}
			break;
	}
}

// //fim do MUSIC STUFF







//volatile Bool but_flag= false;
volatile char happiness=100;
volatile char counter= 0;
volatile char has_something_to_say= 0;
volatile char led_state= 0;
volatile char inf_busy= 0;
volatile char but_busy= 0;

void inf_callback(void){
	
	//fazer algum shield aqui para impedir multiplas chamadas consecutivas
	if(!inf_busy && has_something_to_say){
		inf_busy= 1;
		pio_set(LED_PIO, LED_PIO_PIN_MASK);
		
		//aumentar a felicidade e fazer um som de feedback
		//prevenir overflow
		happiness= ( (happiness+STANDARDINCREASE) < 255) ? (happiness+STANDARDINCREASE) : 255;
		sing(THANKYOUBEEP);
		
		//setups
		has_something_to_say= false;
		counter= 0;
		led_state=0;
		pio_clear(LED_PIO, LED_PIO_PIN_MASK);
		inf_busy= 0;	
	}
}

void but_callback(void){
	
	if(!but_busy && has_something_to_say){
		but_busy= 1;
		pio_set(LED_PIO, LED_PIO_PIN_MASK);
		
		if(happiness>= 100){
			sing(HAPPYBEEP);	
		}else{
			sing(SADBEEP);
		}
		
		//setups
		has_something_to_say= false;
		counter= 0;
		led_state=0;
		pio_clear(LED_PIO, LED_PIO_PIN_MASK);
		but_busy= 0;
	}
}
int main(void)
{	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	//TODO depois habilitar o watchdog, desabilitar por enquanto para testes
	WDT->WDT_MR = WDT_MR_WDDIS;

	//setar os pinos utilizados
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUZ_PIO, PIO_OUTPUT_0, BUZ_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_configure(INF_PIO, PIO_INPUT, INF_PIO_PIN_MASK, PIO_PULLUP);
	
	//pio_set_debounce_filter(BUT_PIO, BUT_PIO_PIN_MASK, 30);
	pio_clear(PIOC, LED_PIO_PIN_MASK);
	
	//ativar o interrupt para receber toques do bot�o
	//	//Button interrupt
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE, but_callback);
	
	//	//Infrared interrupt
	pio_enable_interrupt(INF_PIO, INF_PIO_PIN_MASK);
	pio_handler_set(INF_PIO, INF_PIO_ID, INF_PIO_PIN_MASK, PIO_IT_FALL_EDGE, inf_callback);
	
	/*	//Clock interrupt
	pmc_enable_periph_clk(ID_TC0);

	tc_init(TC0, 0, TC_CMR_TCCLKS_TIMER_CLOCK1 | TC_CMR_CPCTRG | TC_CMR_ACPA_TOGGLE);

	uint32_t ra, rc;

	rc = 25;
	tc_write_rc(TC0, 0,  rc);
	ra = (100-50)*rc/100;
	tc_write_ra(TC0,0,ra);
	
	tc_enable_interrupt(TC0, 0, TC_IER_CPCS | TC_IER_CPAS);
	*/	//NVIC Priorities
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);
	
	NVIC_EnableIRQ(INF_PIO_ID);
	NVIC_SetPriority(INF_PIO_ID, 1);
	
	/*
	NVIC_EnableIRQ(ID_TC0);
	NVIC_SetPriority(ID_TC0, 0);
	*/
	
	while (1) {
		//pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		counter=(counter+1)%10;
		
		has_something_to_say|= !counter; //seta has_something_to_say como true quando counter for zero
		happiness-=!!happiness & !counter; //se happiness for maior que zero e counter for zero, decrementar um
		
		if(has_something_to_say){ //pisca o led
			switch(led_state){
				case 0:
					pio_set(LED_PIO, LED_PIO_PIN_MASK);
					break;
					
				case 1:
					pio_clear(LED_PIO, LED_PIO_PIN_MASK);
					break;
			}
			led_state= !led_state;
		}
		
		delay_ms(HAPPINESSCYCLE);
	}
	return 0;
}
