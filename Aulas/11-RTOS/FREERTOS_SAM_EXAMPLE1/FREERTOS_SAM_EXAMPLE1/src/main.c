#include <asf.h>
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (tskIDLE_PRIORITY)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

/* Botao da placa */
#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

void but_callback(void);
void init_but_board(void);

/** Semaforos da entrega */
SemaphoreHandle_t Semaphore_Led1;
SemaphoreHandle_t Semaphore_Led2;
SemaphoreHandle_t Semaphore_Led3;

// flags for monitor
int flag_blink_1 = 0;
int flag_blink_2 = 0;
int flag_blink_3 = 0;

/* DEFINES DA ENTREGA */

#define LEDX1_FREQ 10 // Hz
#define LEDX2_FREQ 4 // Hz
#define LEDX3_FREQ 1 // Hz

#define LEDX1_PIO_ID ID_PIOA
#define LEDX1_PIO PIOA
#define LEDX1_PIN 0
#define LEDX1_PIN_MASK (1 << LEDX1_PIN)

#define LEDX2_PIO_ID ID_PIOC
#define LEDX2_PIO PIOC
#define LEDX2_PIN 30
#define LEDX2_PIN_MASK (1 << LEDX2_PIN)

#define LEDX3_PIO_ID ID_PIOB
#define LEDX3_PIO PIOB
#define LEDX3_PIN 2
#define LEDX3_PIN_MASK (1 << LEDX3_PIN)

#define BUTX1_PIO_ID ID_PIOD
#define BUTX1_PIO PIOD
#define BUTX1_PIN 28
#define BUTX1_PIN_MASK (1 << BUTX1_PIN)

#define BUTX2_PIO_ID ID_PIOC
#define BUTX2_PIO PIOC
#define BUTX2_PIN 31
#define BUTX2_PIN_MASK (1 << BUTX2_PIN)

#define BUTX3_PIO_ID ID_PIOA
#define BUTX3_PIO PIOA
#define BUTX3_PIN 19
#define BUTX3_PIN_MASK (1 << BUTX3_PIN)


/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */
static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);

	for (;;) {
		printf("\n--- task ## %u", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		printf("--LEDs Statuts:\n");
		printf("Led 1 - %s\n", flag_blink_1 ? "Blinking":"Stopped");
		printf("Led 2 - %s\n", flag_blink_2 ? "Blinking":"Stopped");
		printf("Led 3 - %s\n", flag_blink_3 ? "Blinking":"Stopped");
		vTaskDelay(250);
	}
}

/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */

static void task_led1(void *pvParameters)
{
	Semaphore_Led1 = xSemaphoreCreateBinary();
	
	const TickType_t xDelay = ((1000 /LEDX1_FREQ) / portTICK_PERIOD_MS) / 2; //divide by 2 to get full wave for each period (toggle needs to be called twice)
	
	
	if (Semaphore_Led1 == NULL)
		printf("falha em criar o semaforo \n");

	for (;;) {
		if( xSemaphoreTake(Semaphore_Led1, ( TickType_t ) 500) == pdTRUE ){
			flag_blink_1 = 1;
			while(xSemaphoreTake(Semaphore_Led1, ( TickType_t ) 0) != pdTRUE){
				pin_toggle(LEDX1_PIO, LEDX1_PIN_MASK);
				//printf("blink 1\n");
				vTaskDelay(xDelay);
			}
			flag_blink_1 = 0;
		}
	}
}
static void task_led2(void *pvParameters)
{
	Semaphore_Led2 = xSemaphoreCreateBinary();
	
	const TickType_t xDelay = ((1000 /LEDX2_FREQ) / portTICK_PERIOD_MS) / 2; //divide by 2 to get full wave for each period (toggle needs to be called twice)
	
	if (Semaphore_Led2 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if( xSemaphoreTake(Semaphore_Led2, ( TickType_t ) 500) == pdTRUE ){
			flag_blink_2 = 1;
			while(xSemaphoreTake(Semaphore_Led2, ( TickType_t ) 0) != pdTRUE){
				pin_toggle(LEDX2_PIO, LEDX2_PIN_MASK);
				//printf("blink 2\n");
				vTaskDelay(xDelay);
			}
			flag_blink_2 = 0;
		}
	}
}
static void task_led3(void *pvParameters)
{
	Semaphore_Led3 = xSemaphoreCreateBinary();
	
	const TickType_t xDelay = ((1000 /LEDX3_FREQ) / portTICK_PERIOD_MS) / 2; //divide by 2 to get full wave for each period (toggle needs to be called twice)
	
	if (Semaphore_Led3 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if( xSemaphoreTake(Semaphore_Led3, ( TickType_t ) 500) == pdTRUE ){
			flag_blink_3 = 1;
			while(xSemaphoreTake(Semaphore_Led3, ( TickType_t ) 0) != pdTRUE){
				pin_toggle(LEDX3_PIO, LEDX3_PIN_MASK);
				//printf("blink 3\n");
				vTaskDelay(xDelay);
			}
			flag_blink_3 = 0;
		}
	}
}

void but1_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("\nButton1 was pressed\n");
	xSemaphoreGiveFromISR(Semaphore_Led1, &xHigherPriorityTaskWoken);
}
void but2_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("\nButton2 was pressed\n");
	xSemaphoreGiveFromISR(Semaphore_Led2, &xHigherPriorityTaskWoken);
}
void but3_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("\nButton3 was pressed\n");
    xSemaphoreGiveFromISR(Semaphore_Led3, &xHigherPriorityTaskWoken);
}


void init_but(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* conf bot�o como entrada */
	pmc_enable_periph_clk(id);
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 8);
	pio_configure(pio, PIO_INPUT, mask, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(pio, mask, 60);
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE , handler);
}

void LED_init(){
	pio_set_output(LEDX1_PIO, LEDX1_PIN_MASK, 0, 0, 0);
	pio_set_output(LEDX2_PIO, LEDX2_PIN_MASK, 0, 0, 0);
	pio_set_output(LEDX3_PIO, LEDX3_PIN_MASK, 0, 0, 0);
};



/**
 * \brief Configure the console UART.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();

	/* Output demo information. */
	printf("-- Freertos Super amazing test with 3 LEDs\n\r");
	printf("-- That's right!!! THREE LEDs!!!\n\r");
	printf("-- %s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

	/* iniciliza botao */
	init_but(BUTX1_PIO, BUTX1_PIO_ID, BUTX1_PIN, BUTX1_PIN_MASK, but1_callback);
	init_but(BUTX2_PIO, BUTX2_PIO_ID, BUTX2_PIN, BUTX2_PIN_MASK, but2_callback);
	init_but(BUTX3_PIO, BUTX3_PIO_ID, BUTX3_PIN, BUTX3_PIN_MASK, but3_callback);
	
	LED_init();
	
	/* Create task to monitor processor activity */
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL,
			TASK_MONITOR_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Monitor task\r\n");
	}

	/* Create task to make led blink */
	
	if (xTaskCreate(task_led1, "Led 1", TASK_LED_STACK_SIZE, NULL,
			TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led 1 task\r\n");
	}
	if (xTaskCreate(task_led2, "Led 2", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led 2 task\r\n");
	}
	if (xTaskCreate(task_led3, "Led 3", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led 3 task\r\n");
	}

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
